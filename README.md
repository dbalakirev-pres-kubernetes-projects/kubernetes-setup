# Docker based Kubernetes setup

## Installation

### Setup cluster
> . start-cluster.sh

### Init cli
> . init-env.sh

## Usage

### Run app
kubectl run my-docker-app --image=dbalakirev/static-docker-webapp:v1 --port=80 --replicas=1

### Delete pod
kubectl delete pod my-docker-app-1141018356-uelj7

### Scale
kubectl scale deployment my-docker-app --replicas=3

### Check IP
kubectl describe pod my-docker-app-1141018356-4iv3u my-docker-app-1141018356-hjqc0 my-docker-app-1141018356-jqsie | grep IP
IP:		172.17.0.2
IP:		172.17.0.3
IP:		172.17.0.4

### Expose
kubectl expose deployment my-docker-app

### Rolling update
kubectl edit deployment my-docker-app

### Rollback
kubectl rollout undo deployment/my-docker-app
