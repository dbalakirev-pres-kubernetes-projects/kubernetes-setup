#!/bin/sh
KUBECTL_FILE=./bin/kubectl
if [ -f $KUBECTL_FILE ];
then
   echo "File $KUBECTL_FILE exists."
else
   echo "File $KUBECTL_FILE does not exist > download..."
   curl -sSL "http://storage.googleapis.com/kubernetes-release/release/v1.2.0/bin/linux/amd64/kubectl" > $KUBECTL_FILE
   chmod +x $KUBECTL_FILE
   echo 'Done'
fi
PATH=$PATH:./bin
echo "$KUBECTL_FILE is now on your PATH"
